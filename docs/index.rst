.. DTSA Documentation documentation master file, created by
   sphinx-quickstart on Sat Aug  4 18:57:51 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DTSA Documentation's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   database/index
   faqs/index
   

