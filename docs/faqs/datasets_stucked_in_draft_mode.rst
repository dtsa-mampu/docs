Dataset Stucked in *Draft* Mode
===============================

Sypnosis
--------

Dataset are stucked in “Draft” mode even though its status is *Pending
for Firs Approval* or *Pending for Second Approval*. The said dataset is
also no where to be found in the *Datasets for Approval Screen*.

Symptom
-------

1. Dataset is in *Pending for First Approval* or *Pending for Second
   Approval* state, but its not visible in *Datasets for Approval
   Screen*.
2. Dataset always have the *‘[Draft]’* text at the end of the dataset
   title.

.. figure:: ../_static/img/dataset_pending_approval_draft.png
   :alt: Dataset *Pending for Approval* but in draft

   Dataset *Pending for Approval* but in draft

Reason
------

The dataset’s is stucked in *Draft* mode due to User never *Finishes*
the steps to upload resource. When finished uploading resource(s) for a
particular datasets, User **must** clicked on the **Finish** button.

.. figure:: ../_static/img/datasource_edit_page.png
   :alt: Datasource Edit Page

   Datasource Edit Page

Clicking on *Save and add more* button will result in this situation,
where CKAN failed to acknowledge that the dataset has finished uploading
the resource file.

Resolution
----------

Open the dataset, click on *Next: Add Data*, upload a small file (any
file would do), fill up the forms and click “Finish”. This will resolve
the problem.User can then go to the datasets, select the resource and
“Delete” it from the dataset.
